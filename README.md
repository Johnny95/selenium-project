1. 
- Maximise browser window and navigate to http://automationpractice.com URL
2. 
- On the Home page, click Sign in, enter email and click on the create account.
- Fill in all the fields from the form in order to create an account.
- Verify that user is successfully logged in by verifying that the Sign Out exists at the
right corner of the page
3. 
- Verify that the popular category has 7 products
- Verify that the best sellers category has 7 products
4. 
- Enter “Printed dresses” into the search field and save the result titles into a .txt file.
The .txt file should contain the following:
Printed Summer Dress
Printed Dress
Printed Chiffon Dress
Printed Summer Dress
Printed Dress

5. 
- Click on the first result “Printed Summer Dress” menu and:
- Verify that the Tweet, Share, Google and Pinterest social network buttons are
present
- Verify that this dress is -5% off
- Change quantity to 3, size to M and colour to Blue and click Add to cart
- Verify that the new popup shows quantity, size and colour chosen in the previous
step
- Click on the Proceed to Checkout and finalize the shopping by clicking the Proceed
to Checkout and making at least one verification per page (for e.g. price of the
product, name of the buyer etc.)
- At the last step click Pay by bank wire and click on the I confirm my order button
- Verify that the title “Your order on My Store is complete.” is present.
6. 
- Create a new test with a failed assertion. Take a screenshot and send an email to
an email on test failure. Use this test to check that both
screenshot and email functionality work.
