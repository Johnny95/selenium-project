package FifthTest;

import Base.BaseClass;
import Pages.CheckOutPage;
import Pages.PrintedSummerDressPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FifthTest extends BaseClass {
    private final int quantity = 3;
    private final String color = "Blue";
    private final String size = "M";
    private final String price = "$88.94";
    private final String email = "okkanovichasan@gmail.com";
    private final String pass = "Lenovo95";
    private final String state = "United States";
    private final String phone = "4224132";
    private final String realPrice = "$2.00";
    private final String pageTitle = "Your order on My Store is complete.";

    @Test (priority = 0)
    public void testSocialMedia(){
        PrintedSummerDressPage printedSummerDressPage = new PrintedSummerDressPage(driver);
        homePage.goToPrintedSummerDressPage();
        String socialMedia [] = printedSummerDressPage.getSocialMedia();
        Assert.assertEquals(socialMedia[0], "Tweet" , "Tweet assertion has failed");
        Assert.assertEquals(socialMedia[1], "Share" , "Share assertion has failed");
        Assert.assertEquals(socialMedia[2], "Google+" , "Google+ assertion has failed");
        Assert.assertEquals(socialMedia[3], "Pinterest", "Pinterest assertion has failed");
    }
    @Test (priority = 1)
    public void testDiscount(){
        PrintedSummerDressPage printedSummerDressPage = new PrintedSummerDressPage(driver);
        Assert.assertEquals("-5%", printedSummerDressPage.getDiscount(), "Discount is not correct");
    }
    @Test (priority = 2)
    public void testAddToChart(){
        PrintedSummerDressPage printedSummerDressPage = new PrintedSummerDressPage(driver);
        printedSummerDressPage.addToCart(quantity, size, color);
    }
    @Test(priority = 3)
    public void testCartData(){
        PrintedSummerDressPage printedSummerDressPage = new PrintedSummerDressPage(driver);
        String actualData = color + ", " + size + ", " + String.valueOf(quantity);
        Assert.assertEquals(actualData, printedSummerDressPage.getCartData(), "You don't have correct data in the cart");
    }
    @Test(priority = 4)
    public void testProceedCheckOut() throws InterruptedException {
        PrintedSummerDressPage printedSummerDressPage = new PrintedSummerDressPage(driver);
        printedSummerDressPage.goToCheckOut();
        //Checks whether the price is correct and proceeds
        CheckOutPage checkOutPage = new CheckOutPage(driver);
        Assert.assertEquals(price, checkOutPage.summaryVerificationPrice(), "The price is not correct");
        checkOutPage.proceedSummary();
        //Signs in when the user is not sign in currently
        checkOutPage.signIn(email, pass);
        //Verifies state of delivery and phone of billing address, and proceeds
        Assert.assertEquals(state, checkOutPage.addressVerificationStateDelivery(), "State is not correct in delivery address");
        Assert.assertEquals(phone, checkOutPage.addressVerificationPhoneBilling(), "Phone is not correct in billing address");
        checkOutPage.proceedAddress();
        //Verifies shipping price and proceeds
        Assert.assertEquals(realPrice, checkOutPage.shippingPriceVerification(), "Shipping price is not correct");
        checkOutPage.proceedShipping();
        //Verifies availability and pays by bank wire
        Assert.assertEquals("true", String.valueOf(checkOutPage.checkAvailabilityContinueShopping()), "Continue shopping button not present");
        checkOutPage.payBankWire();
        //Confirms the payment
        checkOutPage.confirmOrder();
        //Verify the page title
        Assert.assertEquals(checkOutPage.verifyTheTitle(), pageTitle, "Your order is not complete");
    }
}
