package Sixthtest;
import Base.BaseClass;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import utility.EmailSender;
import java.io.File;
import java.io.IOException;


public class SixthTest extends BaseClass {

    private final String screenShotPath = "C:\\Users\\HasanOkanovic\\Desktop\\" + "screenshotName" + ".png";
    private final String host = "smtp.gmail.com";


    @Test(priority = 0)
    public void testScreenShotEmail(){
        Assert.assertEquals("failed", homePage.geTitle(), "failed");
    }
    @Test(priority = 1)
    public void validateScreenShotEmail(){
        File file = new File(screenShotPath);
        Assert.assertEquals(file.exists(), true, "Your file does not exist");
        EmailSender emailSender = new EmailSender();
        boolean isSuccess = emailSender.sendEmail(screenShotPath);
        Assert.assertEquals(isSuccess, true, "You have not sent an email");
    }
    @AfterMethod
    public void tearDown(ITestResult result)
    {
        if(ITestResult.FAILURE==result.getStatus())
        {
            takeScreenshot();
        }
    }
    private void takeScreenshot () {
        if (driver instanceof TakesScreenshot) {
            File tempFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                File screenshot = new File(screenShotPath);
                FileUtils.copyFile(tempFile, screenshot);
                System.err.println("Screenshot saved to: " + screenshot.getCanonicalPath());
            } catch (IOException e) {
                throw new Error(e);
            }
        }
    }
}
