package ThirdTest;

import Base.BaseClass;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ThirdTest extends BaseClass {

    private final int number = 7;

    @Test(priority = 0)
    public void testNumberOfPopularAndBestSellers(){
        Assert.assertEquals(number, homePage.getNumberOfPopularElements() , "Number of popular items is not 7");
        Assert.assertEquals(number, homePage.getNumberofBestSellers() , "Number of best sellers items is not 7");
    }
}
