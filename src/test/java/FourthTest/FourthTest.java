package FourthTest;

import Base.BaseClass;
import Pages.PrintedDressesSearchResultPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FourthTest extends BaseClass {

    @Test()
    public void createFileTest(){ ;
        homePage.searchPrintedDresses();
        PrintedDressesSearchResultPage printedDressesSearchResultPage = new PrintedDressesSearchResultPage(driver);
        printedDressesSearchResultPage.createTheFile();
        Assert.assertEquals(printedDressesSearchResultPage.readTheFile(), "Printed Summer Dress\n" +
                "Printed Dress\n" +
                "Printed Chiffon Dress\n" +
                "Printed Summer Dress\n" +
                "Printed Dress\n", "The file is not well written");
    }
}
