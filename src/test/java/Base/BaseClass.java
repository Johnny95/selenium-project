package Base;

import Pages.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class BaseClass {

    public WebDriver driver;
    protected HomePage homePage;

    @BeforeSuite
    public void setUP(){
        System.setProperty("webdriver.chrome.driver", "C:\\\\driver.exe");
        driver= new ChromeDriver();
        driver.manage().window().maximize();
        homePage= new HomePage(driver);
        goHome();
    }

    private void goHome(){
        driver.get("http://automationpractice.com");
    }
    @AfterSuite
   public void closeAll(){
     driver.quit();
   }
}
