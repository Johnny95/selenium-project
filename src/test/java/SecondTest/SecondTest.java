package SecondTest;

import Base.BaseClass;
import Pages.CreateAccountPage;
import Pages.SignInPage;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Arrays;
import java.util.List;


public class SecondTest extends BaseClass {

    /*  0- gender
        1- first name
        2- second name
        3- password
        4- day birth
        5- month birth
        6- year birth
        7- first name address
        8- second name address
        9- address
        10- city
        11- state
        12- zip
        13- phone
        14- alias address */

    String email =RandomStringUtils.randomAlphanumeric(15) + "@gmail.com";
    List<String> data = Arrays.asList("male", "Hasan", "Okanović", "Passw0rd0000", "3", "September", "1995" ,
                                    "Hasan" , "Okanović" , "Hansstrasse 6, 43214 Cologne", "New York", "Florida",
                                    "44242" , " +42424252" , "thisissecond@gmail.com");
    @Test(priority = 0)
    public void testSignUp(){

        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        homePage.goToSignIn();
        SignInPage signInPage = new SignInPage(driver);
        signInPage.insertEmailSignIn(email);
        createAccountPage.populateData(data);
        Assert.assertEquals(createAccountPage.getLogInStatus(), "Sign out", "You are not logged in");
    }
}
