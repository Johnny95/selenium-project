package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class PrintedDressesSearchResultPage {

    private WebDriver driver;
    private By searchResultsID = By.cssSelector("ul[class=\"product_list grid row\"]>li>div>div:nth-of-type(2)>h5>a");
    final private String thePath= "C:\\Users\\HasanOkanovic\\Desktop\\javaprogram.txt";

    public PrintedDressesSearchResultPage (WebDriver driver) {
        this.driver = driver;
    }
    public void createTheFile () {
        Path path = Paths.get(thePath); //creates Path instance
        try
        {
            Path p= Files.createFile(path);     //creates file at specified location
            System.out.println("File Created at Path: "+p);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        try {
            Thread.sleep(500);
            WebDriverWait wait = new WebDriverWait(driver,60);
            wait.until(ExpectedConditions.visibilityOfElementLocated(searchResultsID));
            List<WebElement> listOfElements = driver.findElements(searchResultsID);
            FileWriter myWriter = new FileWriter(thePath);
            for(int i=0; i< listOfElements.size(); i++) {
                myWriter.write(listOfElements.get(i).getText() + "\n");
            }
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException | InterruptedException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
    public String readTheFile(){
        String content = "";
        try {
            File myObj = new File(thePath);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                content+=data +"\n";
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return content;
    }
}
