package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PrintedSummerDressPage {

    private final WebDriver driver;
    private final By tweetID = By.cssSelector("#center_column > div > div > div.pb-center-column.col-xs-12.col-sm-4 > p.socialsharing_product.list-inline.no-print > button.btn.btn-default.btn-twitter");
    private final By shareID = By.cssSelector("#center_column > div > div > div.pb-center-column.col-xs-12.col-sm-4 > p.socialsharing_product.list-inline.no-print > button.btn.btn-default.btn-facebook");
    private final By googleID = By.cssSelector("#center_column > div > div > div.pb-center-column.col-xs-12.col-sm-4 > p.socialsharing_product.list-inline.no-print > button.btn.btn-default.btn-google-plus");
    private final By pinterestID = By.cssSelector("#center_column > div > div > div.pb-center-column.col-xs-12.col-sm-4 > p.socialsharing_product.list-inline.no-print > button.btn.btn-default.btn-pinterest");
    private final By discountID = By.id("reduction_percent");
    private final By quantityID = By.id("quantity_wanted");
    private final By colorBlue = By.id("color_14");
    private final By colorBlack = By.id("color_11");
    private final By colorOrange = By.id("color_13");
    private final By colorYellow = By.id("color_16");
    private final By addToCartButtonID = By.id("add_to_cart");
    private final String selectID = "group_1";
    private final By attributesID = By.id("layer_cart_product_attributes");
    private final By attQuantityID = By.id("layer_cart_product_quantity");
    private final By proceedCheckOutButton= By.cssSelector("#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div.button-container > a");

    public PrintedSummerDressPage(WebDriver driver) {
        this.driver = driver;
    }

    public String[] getSocialMedia() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(tweetID));
        String tweet = driver.findElement(tweetID).getText();
        String share = driver.findElement(shareID).getText();
        String google = driver.findElement(googleID).getText();
        String pintrest = driver.findElement(pinterestID).getText();
        String[] socialMedia = new String[4];
        socialMedia[0] = tweet;
        socialMedia[1] = share;
        socialMedia[2] = google;
        socialMedia[3] = pintrest;
        return socialMedia;
    }
    public String getDiscount() {
        return driver.findElement(discountID).getText();
    }

    public void addToCart(int quantity, String size, String color ) {
        WebElement quantityElement= driver.findElement(quantityID); quantityElement.clear(); quantityElement.sendKeys(String.valueOf(quantity));
        Select sizeSelect = new Select(driver.findElement(By.id(selectID)));
        if(size.equals("S")){
            sizeSelect.selectByValue(String.valueOf(1));
        } else if (size.equals("M")){
            sizeSelect.selectByValue(String.valueOf(2));
        }else if (size.equals("L")){
            sizeSelect.selectByValue(String.valueOf(3));
        }
        switch (color){
            case "Black": driver.findElement(colorBlack).click(); break;
            case "Orange": driver.findElement(colorOrange).click(); break;
            case "Blue": driver.findElement(colorBlue).click(); break;
            case "Yellow": driver.findElement(colorYellow).click(); break;
        }
        driver.findElement(addToCartButtonID).click();
    }
    public String getCartData() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(attQuantityID));
        String colorSize= driver.findElement(attributesID).getText();
        String quantity= driver.findElement(attQuantityID).getText();
        String attributes = colorSize + ", " + quantity;
        return attributes;
    }
    public CheckOutPage goToCheckOut() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(proceedCheckOutButton));
        driver.findElement(proceedCheckOutButton).click();
        return new CheckOutPage(driver);
    }
}
