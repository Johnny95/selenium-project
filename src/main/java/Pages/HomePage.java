package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class HomePage {

    private WebDriver driver;
    private By signinID= By.cssSelector("#header > div.nav > div > div > nav > div.header_user_info > a");
    private By searchBoxID= By.id("search_query_top");
    private By firstItemID = By.cssSelector("#center_column > ul > li.ajax_block_product.col-xs-12.col-sm-6.col-md-4.first-in-line.last-line.first-item-of-tablet-line.first-item-of-mobile-line.last-mobile-line > div");
    private By popularProductsID = By.xpath("//ul[@id=\"homefeatured\"]/li");
    private By bestSellersButtonID = By.cssSelector("#home-page-tabs > li:nth-child(2)");
    private By bestSellersID = By.xpath("//ul[@id=\"homefeatured\"]/li");

    public HomePage(WebDriver driver){
        this.driver = driver;
    }
    public SignInPage goToSignIn() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(signinID));
        driver.findElement(signinID).click();
        return new SignInPage(driver);
    }
    public PrintedSummerDressPage goToPrintedSummerDressPage(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchBoxID));
        driver.findElement(searchBoxID).sendKeys("Printed Summer Dress", Keys.ENTER);
        WebDriverWait wait2 = new WebDriverWait(driver,30);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(firstItemID));
        driver.findElement(firstItemID).click();
        return new PrintedSummerDressPage(driver);
    }
    public PrintedDressesSearchResultPage searchPrintedDresses () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchBoxID));
        driver.findElement(searchBoxID).sendKeys("Printed dresses", Keys.ENTER);
        return new PrintedDressesSearchResultPage(driver);
    }
    public int getNumberOfPopularElements(){
       List<WebElement> popularElements =  driver.findElements(popularProductsID);
       return popularElements.size();
    }
    public int getNumberofBestSellers(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(bestSellersButtonID));
        driver.findElement(bestSellersButtonID).click();
        List<WebElement> bestSellers = driver.findElements(bestSellersID);
        return bestSellers.size();
    }
    public String geTitle() {
        return driver.getTitle();
    }
}
