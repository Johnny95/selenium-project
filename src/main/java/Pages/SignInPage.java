package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignInPage {

    private WebDriver driver;
    private By emailInputFieldID = By.id("email_create");
    private By createAccButtonID = By.id("SubmitCreate");

    public SignInPage(WebDriver driver){
        this.driver = driver;
    }

    public CreateAccountPage insertEmailSignIn(String email){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(emailInputFieldID));
        driver.findElement(emailInputFieldID).sendKeys(email);
        driver.findElement(createAccButtonID).click();
        return new CreateAccountPage (driver);
    }
}
